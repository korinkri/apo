#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "board.h"


/**
 * @brief Initializes a field (Field) on the game board.
 * 
 * @param field Pointer to the field to be initialized.
 * @param x X coordinate of the field.
 * @param y Y coordinate of the field.
 * @param is_mine Determines whether the field is a mine.
 */
void init_field(Field *field, int x, int y, bool is_mine) {
    field->positionX = x;
    field->positionY = y;
    field->is_mine = is_mine;
    field->flagged = false;
    field->uncovered = true;
    field->visibleMineCount = false;
    field->borderingMinesCount = 0;
}

/**
 * @brief Initializes the game board (Board) with the given dimension.
 * 
 * @param board Pointer to the game board to be initialized.
 * @param dimension Dimension of the game board (8 or 16).
 */
void init_board(Board *board, int dimension) {
    board->dimension = dimension;
    board->minesCount = dimension == 8 ? 10 : 40;
    board->pointer[0] = 0;
    board->pointer[1] = 0;
    board->fields_to_uncover = dimension * dimension - board->minesCount;
    board->directions[0][0] = -1; board->directions[0][1] = -1;
    board->directions[1][0] = -1; board->directions[1][1] = 0;
    board->directions[2][0] = -1; board->directions[2][1] = 1;
    board->directions[3][0] = 0; board->directions[3][1] = 1;
    board->directions[4][0] = 1; board->directions[4][1] = 1;
    board->directions[5][0] = 1; board->directions[5][1] = 0;
    board->directions[6][0] = 1; board->directions[6][1] = -1;
    board->directions[7][0] = 0; board->directions[7][1] = -1;

    board->board = (Field**)malloc(dimension * sizeof(Field*));
    for (int i = 0; i < dimension; i++) {
        board->board[i] = (Field*)malloc(dimension * sizeof(Field));
        for (int j = 0; j < dimension; j++) {
            init_field(&board->board[i][j], i, j, false);
        }
    }
}

/**
 * @brief Generates mines on the game board.
 * 
 * @param board Pointer to the game board where mines are to be generated.
 */
void generate_mines(Board *board) {
    int dimension = board->dimension;
    int minesCount = board->minesCount;
    int placedMines = 0;

    srand(time(NULL));
    while (placedMines < minesCount) {
        int i = rand() % dimension;
        int j = rand() % dimension;
        if (!board->board[i][j].is_mine) {
            board->board[i][j].is_mine = true;
            placedMines++;
        }
    }
}

/**
 * @brief Checks if the coordinates are on the game board.
 * 
 * @param board Pointer to the game board.
 * @param x X coordinate.
 * @param y Y coordinate.
 * @return true if the coordinates are on the game board, otherwise false.
 */
bool is_on_board(Board *board, int x, int y) {
    return x >= 0 && x < board->dimension && y >= 0 && y < board->dimension;
}

/**
 * @brief Counts the number of mines surrounding each field on the game board.
 * 
 * @param board Pointer to the game board.
 */
void count_bordering_mines(Board *board) {
    for (int x = 0; x < board->dimension; x++) {
        for (int y = 0; y < board->dimension; y++) {
            Field *field = &board->board[x][y];
            for (int d = 0; d < 8; d++) {
                int new_x = x + board->directions[d][0];
                int new_y = y + board->directions[d][1];
                if (is_on_board(board, new_x, new_y)) {
                    Field *neighbour = &board->board[new_x][new_y];
                    if (neighbour->is_mine) {
                        field->borderingMinesCount++;
                    }
                }
            }
        }
    }
}

/**
 * @brief Generates the game board with all fields and mines.
 * 
 * @param board Pointer to the game board.
 */
void generate_board(Board *board) {
    generate_mines(board);
    count_bordering_mines(board);
}

/**
 * @brief Uncovers neighboring fields if they are empty (have no neighboring mines).
 * 
 * @param board Pointer to the game board.
 * @param field Pointer to the field whose neighbors are to be uncovered.
 */
void visualize_neighbours(Board *board, Field *field) {
    int x = field->positionX;
    int y = field->positionY;

    for (int d = 0; d < 8; d++) {
        int new_x = x + board->directions[d][0];
        int new_y = y + board->directions[d][1];

        if (is_on_board(board, new_x, new_y)) {
            Field *neighbour = &board->board[new_x][new_y];
            if (!neighbour->is_mine && neighbour->uncovered) {
                neighbour->uncovered = false;
                board->fields_to_uncover--;

                if (neighbour->borderingMinesCount == 0) {
                    visualize_neighbours(board, neighbour);
                }
            }
        }
    }
}


/**
 * @brief Moves the pointer on the game board horizontally.
 * 
 * @param board Pointer to the game board.
 * @param direction Direction of the movement (positive or negative).
 */
void move_pointer_horizontally(Board *board, int direction) {
    if (board->dimension > board->pointer[1] + direction && board->pointer[1] + direction >= 0) {
        board->pointer[1] += direction;
    }
}

/**
 * @brief Moves the pointer on the game board vertically.
 * 
 * @param board Pointer to the game board.
 * @param direction Direction of the movement (positive or negative).
 */
void move_pointer_vertically(Board *board, int direction) {
    if (board->dimension > board->pointer[0] + direction && board->pointer[0] + direction >= 0) {
        board->pointer[0] += direction;
    }
}

/**
 * @brief Flags or unflags a field as containing a mine.
 * 
 * @param board Pointer to the game board.
 */
void flag_field(Board *board) {
    int x = board->pointer[0];
    int y = board->pointer[1];
    board->board[x][y].flagged = !board->board[x][y].flagged;
}

/**
 * @brief Uncovers the field at the current pointer position.
 * 
 * @param board Pointer to the game board.
 * @return -1 if a mine is uncovered, 1 if all safe fields are uncovered, otherwise 0.
 */
int uncover_field(Board *board) {
    int x = board->pointer[0];
    int y = board->pointer[1];
    Field *field = &board->board[x][y];

    if (field->uncovered) {
        if (field->is_mine) {
            print_uncovered(board);
            return -1;
        } else {
            field->uncovered = false;
            board->fields_to_uncover--;
            if (field->borderingMinesCount == 0) {
                visualize_neighbours(board, field);
            }
            if (board->fields_to_uncover == 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    return 0;
}

/**
 * @brief Prints the game board with covered or uncovered fields.
 * 
 * @param board Pointer to the game board to be printed.
 */
void print_board(Board *board) {
    for (int x = 0; x < board->dimension; x++) {
        for (int y = 0; y < board->dimension; y++) {
            if (board->pointer[0] == x && board->pointer[1] == y) {
                printf(" [%c] ", board->board[x][y].uncovered ? (board->board[x][y].flagged ? 'F' : '.') : '0' + board->board[x][y].borderingMinesCount);
            } else {
                printf("  %c  ", board->board[x][y].uncovered ? (board->board[x][y].flagged ? 'F' : '.') : '0' + board->board[x][y].borderingMinesCount);
            }
        }
        printf("\n");
    }
    printf("\n");	
}

/**
 * @brief Prints the game board with uncovered fields showing mines or mine counts.
 * 
 * @param board Pointer to the game board to be printed.
 */
void print_uncovered(Board *board) {
    for (int x = 0; x < board->dimension; x++) {
        for (int y = 0; y < board->dimension; y++) {
            if (board->pointer[0] == x && board->pointer[1] == y) {
                printf(" [%c] ", board->board[x][y].is_mine ? 'm' : '0' + board->board[x][y].borderingMinesCount);
            } else {
                printf("  %c  ", board->board[x][y].is_mine ? 'm' : '0' + board->board[x][y].borderingMinesCount);
            }
        }
        printf("\n");
    }
}
