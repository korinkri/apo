/*******************************************************************
  Project main function from template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON
 
  Bielova Karolina, Korinkova Kristyna
 
 *******************************************************************/
#include "controller.h"

int main(int argc, char *argv[]) {
	
  handle_controls();	
 
  return 0;
}
