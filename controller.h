#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "board.h"

void handle_controls();
int handle_menu_1();
int handle_menu_2();
void handle_menu_end_game();
int handle_game(Board *board);

void handle_blue_knob(Board *board, int new_blue);
void handle_red_knob(Board *board, int new_red);

int handle_blue_press(Board *board);
void handle_red_press(Board *board);

void reset_knobs();

int handle_state_change(int state);

void print_menu_1();
void print_menu_2();

#endif // CONTROLLER_H
