#ifndef VIEW_H
#define VIEW_H
#include "board.h"

void init();
int view();

void draw_board(Board *board, unsigned char *parlcd_mem_base);
void draw_board_ucovered(Board *board, unsigned char *parlcd_mem_base);
void draw_menu_1(int option, unsigned char *parlcd_mem_base);
void draw_menu_2(int option, unsigned char *parlcd_mem_base);
void draw_blank(unsigned char *parlcd_mem_base);
void draw_win(Board *board, unsigned char *parlcd_mem_base);
void draw_game_over(Board *board, unsigned char *parlcd_mem_base);
unsigned int rgb_to_16bit(int r, int g, int b);
void draw_pixel(int x, int y, unsigned short color);
void draw_pixel_big(int x, int y, unsigned short color);
int char_width(int ch);
void draw_char(int x, int y, char ch, unsigned short color);
void clear();

#endif // VIEW_H
