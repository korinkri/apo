#ifndef BOARD_H
#define BOARD_H

#include <stdbool.h>

typedef struct {
    int positionX;
    int positionY;
    bool is_mine;
    bool flagged;
    bool uncovered;
    bool visibleMineCount;
    int borderingMinesCount;
} Field;

typedef struct {
    Field **board;
    int dimension;
    int minesCount;
    int pointer[2];
    int fields_to_uncover;
    int directions[8][2];
} Board;

void init_board(Board *board, int dimension);
void generate_board(Board *board);
void move_pointer_horizontally(Board *board, int direction);
void move_pointer_vertically(Board *board, int direction);
void flag_field(Board *board);
int uncover_field(Board *board);
void print_board(Board *board);
void print_uncovered(Board *board);

#endif // BOARD_H
