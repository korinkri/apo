#include <stdio.h>
#include "mzapo_parlcd.h"
#include "font_types.h"
#include "font_rom8x16.c"
#include "view.h"
#include <linux/string.h>

/**
 * @brief Converts RGB color values to 16-bit color format.
 * 
 * @param r Red color value (0-255).
 * @param g Green color value (0-255).
 * @param b Blue color value (0-255).
 * @return Unsigned integer representing the 16-bit color.
 */
unsigned int rgb_to_16bit(int r, int g, int b) {
  if (r < 0) r = 0; else if (r > 255) r = 255;
  if (g < 0) g = 0; else if (g > 255) g = 255;
  if (b < 0) b = 0; else if (b > 255) b = 255;
  r >>= 3;
  g >>= 2;
  b >>= 3;
  return (((r & 0x1f) << 11) | ((g & 0x3f) << 5) | (b & 0x1f));
}

// Global variables
unsigned short fb[320*480*2];
font_descriptor_t *fdes;
int scale;
int ptr, x, y, i, j, start_x, start_y;
unsigned int color, pointer_color, warning_color, attention_color, col;

/**
 * @brief Initializes various variables used in the program.
 */
void init(){
	scale = 2;
	ptr = 0;
	x = 0;
	y = 0;
	color = rgb_to_16bit(255, 255, 255);
	pointer_color = rgb_to_16bit(0, 80, 255);
	warning_color = rgb_to_16bit(255, 0, 0);
	attention_color = rgb_to_16bit(252, 236, 3);
	fdes = &font_rom8x16;
}

/**
 * @brief Draws a pixel with scaled dimensions on the framebuffer.
 * 
 * @param x X-coordinate of the pixel.
 * @param y Y-coordinate of the pixel.
 * @param color Color of the pixel in 16-bit format.
 */
void draw_pixel(int x, int y, unsigned short color) {
  if (x>=0 && x<480 && y>=0 && y<320) {
    fb[x+480*y] = color;
  }
}

/**
 * @brief Determines the width of a character in pixels.
 * 
 * @param ch ASCII value of the character.
 * @return Width of the character.
 */ 
void draw_pixel_big(int x, int y, unsigned short color) {
  int i,j;
  for (i=0; i<scale; i++) {
    for (j=0; j<scale; j++) {
      draw_pixel(x+i, y+j, color);
    }
  }
}

/**
 * @brief Determines the width of a character in pixels.
 * 
 * @param ch ASCII value of the character.
 * @return Width of the character.
 */
int char_width(int ch) {
  int width;
  if (!fdes->width) {
    width = fdes->maxwidth;
  } else {
    width = fdes->width[ch-fdes->firstchar];
  }
  return width;
}
 
/**
 * @brief Draws a character on the framebuffer.
 * 
 * @param x X-coordinate of the character.
 * @param y Y-coordinate of the character.
 * @param ch ASCII value of the character.
 * @param color Color of the character in 16-bit format.
 */
void draw_char(int x, int y, char ch, unsigned short color) {
    int w = char_width(ch);
    const font_bits_t *ptr;
    
    if ((ch >= fdes->firstchar) && (ch - fdes->firstchar < fdes->size)) {
        if (fdes->offset) {
            ptr = &fdes->bits[fdes->offset[ch - fdes->firstchar]];
        } else {
            int bw = (fdes->maxwidth + 15) / 16;
            ptr = &fdes->bits[(ch - fdes->firstchar) * bw * fdes->height];
        }
        
        int i, j;
        for (i = 0; i < fdes->height; i++) {
            const font_bits_t *row_ptr = ptr + i * ((w + 15) / 16);
            int bit_pos = 0;
            
            for (j = 0; j < w; j++) {
                int byte_offset = bit_pos / 16;
                int bit_offset = 15 - (bit_pos % 16);
                
                if ((row_ptr[byte_offset] & (1 << bit_offset)) != 0) {
                    draw_pixel_big(x + scale * j, y + scale * i, color);
                }
                
                bit_pos++;
            }
        }
    }
}

/**
 * @brief Draws the game board on the LCD screen.
 * 
 * @param board Pointer to the game board.
 * @param parlcd_mem_base Base address of the parallel LCD memory.
 */
void draw_board(Board *board, unsigned char *parlcd_mem_base) {
	ptr = 0; 
	clear();
	int s_x = board->dimension == 8 ? 88 : 80;
	int s_y = board->dimension == 8 ? 8 : 5;
	int x_shift = board->dimension == 8 ? 40 : 20;
	int y_shift = board->dimension == 8 ? 40 : 20;
	scale  = board->dimension == 8 ? 2 : 1;
	
    start_y = s_y;
	for (x = 0; x < board->dimension; x++) {
		start_x = s_x;
		for (y = 0; y < board->dimension; y++) {			
			char ch;
			if (board->pointer[0] == x && board->pointer[1] == y) { 
				ch = board->board[x][y].uncovered ? (board->board[x][y].flagged ? 'F' : '#') : '0' + board->board[x][y].borderingMinesCount;
				draw_char(start_x , start_y, ch, pointer_color);
			} else {
				col = board->board[x][y].uncovered && board->board[x][y].flagged ? attention_color  : color;
				ch = board->board[x][y].uncovered ? (board->board[x][y].flagged ? 'F' : '#') : '0' + board->board[x][y].borderingMinesCount;
				draw_char(start_x, start_y, ch, col);  
			}
			start_x += x_shift;  
		}
		start_y += y_shift; 
	}
      for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
}

/**
 * @brief Draws the game board with uncovered fields.
 * 
 * @param board Pointer to the game board.
 * @param parlcd_mem_base Base address of the parallel LCD memory.
 */
void draw_board_ucovered(Board *board, unsigned char *parlcd_mem_base) {
	ptr = 0;
	clear();
	int s_x = board->dimension == 8 ? 88 : 80;
	int s_y = board->dimension == 8 ? 8 : 5;
	int x_shift = board->dimension == 8 ? 40 : 20;
	int y_shift = board->dimension == 8 ? 40 : 20;
	scale  = board->dimension == 8 ? 2 : 1;
	
    start_y = s_y;
	for (x = 0; x < board->dimension; x++) {
		start_x = s_x;
		for (y = 0; y < board->dimension; y++) {			
			char ch;
			ch = board->board[x][y].is_mine ? 'Z' : '0' + board->board[x][y].borderingMinesCount;
			col = board->board[x][y].is_mine ? warning_color : color;
			draw_char(start_x, start_y, ch, col);
			start_x += x_shift; 	  
		}
		start_y += y_shift; 
	}
      for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
}    


/**
 * @brief Draws the main menu of the game.
 * 
 * @param option Currently selected menu option.
 * @param parlcd_mem_base Base address of the parallel LCD memory.
 */
void draw_menu_1(int option, unsigned char *parlcd_mem_base) {
	start_x = 30;
	start_y = 30;
	char *new = "NEW GAME";
	char *exit = "EXIT";
	scale = 2;
	
	ptr = 0;
	clear();
	
	if (option == 0) {
		for (i = 0; i < strlen(new); i++){
			draw_char(start_x , start_y, new[i], pointer_color);
			start_x += 40;
		}
		start_y += 40;
		start_x = 30;
		for (i = 0; i < strlen(exit); i++){
			draw_char(start_x , start_y, exit[i], color);
			start_x += 40;
		}

	}else {
		for (i = 0; i < strlen(new); i++){
			draw_char(start_x , start_y, new[i], color);
			start_x += 40;
		}
		start_y += 40;
		start_x = 30;
		for (i = 0; i < strlen(exit); i++){
			draw_char(start_x , start_y, exit[i], pointer_color);
			start_x += 40;
		}
	}

      for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
} 

/**
 * @brief Draws the difficulty selection menu of the game.
 * 
 * @param option Currently selected menu option.
 * @param parlcd_mem_base Base address of the parallel LCD memory.
 */
void draw_menu_2(int option, unsigned char *parlcd_mem_base) {
	start_x = 30;
	start_y = 30;
	char *easy = "EASY 8x8";
	char *hard = "HARD 16x16";
	char *back = "BACK";
	scale = 2;
	
	ptr = 0;
	clear();
	
	if (option == 0) {
		for (i = 0; i < strlen(easy); i++){
			draw_char(start_x , start_y, easy[i], pointer_color);
			start_x += 40;
		}
		start_y += 40;
		start_x = 30;
		for (i = 0; i < strlen(hard); i++){
			draw_char(start_x , start_y, hard[i], color);
			start_x += 40;
		}
		start_y += 40;
		start_x = 30;
		for (i = 0; i < strlen(back); i++){
			draw_char(start_x , start_y, back[i], color);
			start_x += 40;
		}

	} else if (option == 1) {
		for (i = 0; i < strlen(easy); i++){
			draw_char(start_x , start_y, easy[i], color);
			start_x += 40;
		}
		start_y += 40;
		start_x = 30;
		for (i = 0; i < strlen(hard); i++){
			draw_char(start_x , start_y, hard[i], pointer_color);
			start_x += 40;
		}
		start_y += 40;
		start_x = 30;
		for (i = 0; i < strlen(back); i++){
			draw_char(start_x , start_y, back[i], color);
			start_x += 40;
		}
	}else {
		for (i = 0; i < strlen(easy); i++){
			draw_char(start_x , start_y, easy[i], color);
			start_x += 40;
		}
		start_y += 40;
		start_x = 30;
		for (i = 0; i < strlen(hard); i++){
			draw_char(start_x , start_y, hard[i], color);
			start_x += 40;
		}
		start_y += 40;
		start_x = 30;
		for (i = 0; i < strlen(back); i++){
			draw_char(start_x , start_y, back[i], pointer_color);
			start_x += 40;
		}
	}

      for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
}

/**
 * @brief Draws the win screen.
 * 
 * @param board Pointer to the game board.
 * @param parlcd_mem_base Base address of the parallel LCD memory.
 */
void draw_win(Board *board, unsigned char *parlcd_mem_base) {

	draw_board_ucovered(board, parlcd_mem_base);
	start_x = 30;
	start_y = 100;
	char *mess = "WINWIN";
	scale = 2;

	ptr = 0;

	for (i = 0; i < strlen(mess); i++){
		if (i == 3) {
			start_x += 390;
			start_y = 100;
		}
		draw_char(start_x , start_y, mess[i], attention_color);
		start_y += 40;
	}


      for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
} 

/**
 * @brief Draws the game over screen.
 * 
 * @param board Pointer to the game board.
 * @param parlcd_mem_base Base address of the parallel LCD memory.
 */
void draw_game_over(Board *board, unsigned char *parlcd_mem_base) {
	draw_board_ucovered(board, parlcd_mem_base);
	int start_x = 30;
	int start_y = 80;
	char *mess = "GAMEOVER";
	scale = 2;
	
	ptr = 0;
	
	
	for (i = 0; i < strlen(mess); i++){
		if (i == 4) {
			start_x += 390;
			start_y = 80;
		}
		draw_char(start_x , start_y, mess[i], warning_color);
		start_y += 40;
	}

      for (ptr = 0; ptr < 480*320 ; ptr++) {
        parlcd_write_data(parlcd_mem_base, fb[ptr]);
      }
} 

/**
 * @brief Draws a blank screen on the framebuffer.
 * 
 * @param parlcd_mem_base Base address of the parallel LCD memory.
 */
void draw_blank(unsigned char *parlcd_mem_base){
	for (x = 0; x < 320 ; x++) {
		for (y = 0; y < 480 ; y++) {
			fb[ptr]=0;
			parlcd_write_data(parlcd_mem_base, fb[ptr++]);
		}
	}
}

/**
 * @brief Clears the framebuffer.
 */
void clear() {
	for (x = 0; x < 320 ; x++) {
		for (y = 0; y < 480 ; y++) {
			fb[ptr]=0;
			ptr++;
		}
	}
}
