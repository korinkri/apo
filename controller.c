#include "controller.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"	
#include "font_types.h"

#include "board.h"
#include "view.h"
#include <stdbool.h>

// Global variables	
int32_t prev_knob_value = 0;
int8_t prev_knob_value_blue = 0;
int8_t prev_knob_value_green = 0;
int8_t prev_knob_value_red = 0;
int32_t knob_value;

int stage = 0; // 0=main menu, 1=choose diffuculty, 2=game, 3 = won game, 4= lost game
int option1; // 0=new game 1=exit
int option2; // 0=8x8 1=16x16, 2=back to menu 1
int board_size;
bool playing = 0;
Board board;

/**
 * @brief Prints the main menu screen with options.
 */
void print_menu_1() {
	if (option1== 0) {
		printf("***************\n");
		printf("[NEW GAME]\n");
		printf("EXIT\n");
	} else {
		printf("***************\n");
		printf("NEW GAME\n");
		printf("[EXIT]\n");
	}
}

/**
 * @brief Prints the difficulty selection menu screen with options.
 */
void print_menu_2() {
	if (option2 == 0) {
		printf("***************\n");
		printf("[EASY 8x8]\n");
		printf("HARD 16x16\n");
		printf("BACK\n");
	} else if (option2 == 1) {
		printf("***************\n");
		printf("EASY 8x8\n");
		printf("[HARD 16x16]\n");
		printf("BACK\n");
	}else {
		printf("***************\n");
		printf("EASY 8x8\n");
		printf("HARD 16x16\n");
		printf("[BACK]\n");
	}
}

/**
 * @brief Handles the game controls, including menu navigation and gameplay actions.
 */
void handle_controls() {
	//setup
	
	unsigned char *mem_base;
	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
	if (mem_base == 0) {
			exit(1);	
	}
	
	unsigned char *parlcd_mem_base;
	parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
	if (parlcd_mem_base == NULL)
	exit(1);
	init();
	parlcd_hx8357_init(parlcd_mem_base);
	parlcd_write_cmd(parlcd_mem_base, 0x2c);
	
	print_menu_1(0);
	draw_menu_1(0, parlcd_mem_base);
	
	knob_value = *(volatile int32_t *) (mem_base + SPILED_REG_KNOBS_8BIT_o);
	reset_knobs();
	while(1) {
		knob_value = *(volatile int32_t *) (mem_base + SPILED_REG_KNOBS_8BIT_o);
		if (prev_knob_value != knob_value) {
			prev_knob_value = knob_value;
			
			if (stage == 0) {
				print_menu_1();
				draw_menu_1(option1, parlcd_mem_base);
				int option = handle_menu_1();
				if (option == 100) {
					draw_blank(parlcd_mem_base);
					exit(0);
				}
				if (option != option1) {
					option1 = option;
				}
				print_menu_1();
				draw_menu_1(option1, parlcd_mem_base);
			} else if (stage == 1) {
				print_menu_2();
				draw_menu_2(option2, parlcd_mem_base);
				int option = handle_menu_2();
				if (option != option2) {
					option1 = option;
				}
				print_menu_2();
				draw_menu_2(option2, parlcd_mem_base);
			} else if (stage == 2) {
				if (!playing) {
					init_board(&board, board_size);
					generate_board(&board);
				}
				print_board(&board);
				draw_board(&board, parlcd_mem_base);
				int state = handle_game(&board);
				if (state != 0)
					stage = handle_state_change(state);
				draw_board(&board, parlcd_mem_base);
			} else if (stage == 3) {
				draw_win(&board, parlcd_mem_base);
				handle_menu_end_game();
				print_uncovered(&board);
			} else if (stage == 4) {
				draw_game_over(&board, parlcd_mem_base);	
				print_uncovered(&board);
				handle_menu_end_game();
			}
		}	
	}	
}

/**
 * @brief Handles input and navigation for the main menu.
 * 
 * @return The selected option.
 */
int handle_menu_1(){
	int8_t  new_green;
	bool new_pressed_green;
	new_green = ((knob_value >> 8) & 0xFF) / 4;
	new_pressed_green = (knob_value >> 25) & 1;

	if (new_pressed_green != 0) {

		if (option1 == 0) {
			stage = 1;
			reset_knobs();
		} else {
			return 100;
		}
	}
	if (prev_knob_value_green != new_green) {
		if (prev_knob_value_green > new_green) {
			option1 = 0;
		} else {
			option1 = 1;
		}
		prev_knob_value_green = new_green;
	}
	return option1;		
}


/**
 * @brief Handles input and navigation for the difficulty selection menu.
 * 
 * @return The selected option.
 */
int handle_menu_2(){
	int8_t  new_green;
	bool new_pressed_green;
	new_green= ((knob_value >> 8) & 0xFF) / 4;
	new_pressed_green = (knob_value >> 25) & 1;
	
	
	
	if (new_pressed_green != 0) {
		stage = 2;
		reset_knobs();
		switch(option2){
			case 0:
				board_size = 8;
				break;
			case 1:
				board_size = 16;
				break;
			case 2:
				stage = 0;
				break;
		}
	}
	else if (prev_knob_value_green != new_green) {
		if (prev_knob_value_green < new_green) {
			if (option2 + 1 <= 2)
				option2++;
		} else {
			if (option2 - 1 >= 0)
				option2--;
		}
		prev_knob_value_green = new_green;
	}

	return option2;		
}

/**
 * @brief Handles actions when the game ends and the menu needs to be displayed.
 */
void handle_menu_end_game() {
	bool new_pressed_green;
	new_pressed_green = (knob_value >> 25) & 1;

	if (new_pressed_green != 0) {
		stage = 0;
	}
	reset_knobs();
}

/**
 * @brief Handles the gameplay logic based on knob input and button presses.
 * 
 * @param board Pointer to the game board.
 * @return The next stage of the game.
 */
int handle_game(Board *board) {
	int8_t  new_blue;
	int8_t  new_red;
	bool new_pressed_blue;
	bool new_pressed_red;
	bool new_pressed_green;
	
	new_blue = (knob_value & 0xFF) / 4;
	new_red = ((knob_value >> 16) & 0xFF) / 4;	
	new_pressed_blue = (knob_value >> 24) & 1;
	new_pressed_red = (knob_value >> 26) & 1;
	new_pressed_green = (knob_value >> 25) & 1;
	
	if(!playing) {
		prev_knob_value_blue = new_blue;
		prev_knob_value_red = new_red;
		playing = 1;
	}
	
	if (prev_knob_value_blue != new_blue) {
		handle_blue_knob(board, new_blue);
		prev_knob_value_blue = new_blue;
	} else if (prev_knob_value_red != new_red) {
		handle_red_knob(board, new_red);
		prev_knob_value_red = new_red;
	} else if (new_pressed_blue != 0) {
		return handle_blue_press(board);
	} else if (new_pressed_red != 0) {
		handle_red_press(board);
		sleep(0.5);
	} else if (new_pressed_green != 0) {
		return 10;
	}
	return 0;
}

/**
 * @brief Handles movement of the pointer based on blue knob input.
 * 
 * @param board Pointer to the game board.
 * @param new_blue The new value of the blue knob.
 */
void handle_blue_knob(Board *board, int new_blue){
	if (prev_knob_value_blue > new_blue)
		move_pointer_vertically(board,-1);
	else
		move_pointer_vertically(board,1);	
}

/**
 * @brief Handles movement of the pointer based on red knob input.
 * 
 * @param board Pointer to the game board.
 * @param new_red The new value of the red knob.
 */
void handle_red_knob(Board *board, int new_red){
	if (prev_knob_value_red > new_red)
		move_pointer_horizontally(board,-1);
	else
		move_pointer_horizontally(board, 1);
}

/**
 * @brief Handles the action when the blue button is pressed during gameplay.
 * 
 * @param board Pointer to the game board.
 * @return The state change after the action.
 */
int handle_blue_press(Board *board){
	return uncover_field(board);
}

/**
 * @brief Handles the action when the red button is pressed during gameplay.
 * 
 * @param board Pointer to the game board.
 */
void handle_red_press(Board *board){
	flag_field(board);
}


/**
 * @brief Handles state changes during gameplay and returns the next stage.
 * 
 * @param state The current state of the game.
 * @return The next stage of the game.
 */
int handle_state_change(int state){
	playing = 0;
	reset_knobs();
	switch(state) {
			case 1:
				printf("You uncovered all fields. Game won\n");
				return 3;
			case -1:
				printf("You uncovered a mine. Game over\n");
				return 4;
			case 10:
				printf("Return to menu\n");
				return 0;		
	}
	return 0;
}


/**
 * @brief Resets the previous knob values to the current knob values.
 */
void reset_knobs(){
	prev_knob_value_blue= (knob_value & 0xFF) / 4;
	prev_knob_value_green = ((knob_value >> 8) & 0xFF) / 4;
	prev_knob_value_red = ((knob_value >> 16) & 0xFF) / 4;	
}
